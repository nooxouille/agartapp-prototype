# Internship mission
*  create an augmented reality app.
*  show-off some of the products made by the Agarta team (https://www.agarta-agency.fr/).
*  3D models of the products


# Technologies
* Unity Engine + C#
* PHP + MySQL in future version

# Work In Progress

The app is still a prototype, with an ultra basic database connection, as I am still learning how to master both Unity and C# language. <br/>
Loads of plans for the future, including building a solid back-office (*for a more custom app*) and improving the AR functionnalities.

The user-interface was made using the DoozyUI asset (v3.0), the Augmented Reality part using Vuforia 8.3. <br/>
I delt with the touch-inputs using the LeanTouch+ assets<br/> 
The most "home-made" functionnality is the carousel one, allowing the user to switch from one 3D model to the other.<br/>
The 3D models were made using C4D (Cinema4D) by a co-worker. But for some reason, I had to re-textured ALL of the bottles inside Unity. Good training tho, as I had to learned how-to do so.

# What's up next ?

As of today, the project is set on hold : the initial goal was to work on a prototype, and decide or not to push it further at the end of my internship.<br/>
Because I was the only developer on the project (*whith an **impressive** 7 months's background into developement as an almost self-learner*), it took some time to achieve it. <br/>
<br/>
This version of the project is actually a version "2" : cleaner, with a better structure even thought it is not as polish as I'd like. <br/>
A lot of work need to be done, especially on the AR part and the back-office.<br/>
But because I am currently looking for a job, and because I didn't get paid [^1] for that work (which represented many hours for the beginner I am), the project is on hold for the moment.<br>
My goal is to be able, within next year, to deliver a complete app showing off the stuff made by the agency as imagined during my internship.<br/>
To do so, I must keep improving on my **Unity** & **C#** skills on smaller and more personal projects (*amateur video-games tbh*)

# Screenshots

![The application's splash-screen](/sources/images/splash-screen.png)

*The actual's splash-screen version is static, but it is plan to make it interactive*

![The application's main-menu, or main-hub](/sources/images/main-menu.png) 

*The main-menu is as close as possible to the latest mockup*

![The application's sub-menu](/sources/images/sub-menu.png)

*That blurry effect on the sub-menu took-off some of my hairs*

![Soap packaging example](/sources/images/carousel-one.png)

*3D model of a soap-box inside the packagings carousel*

![E-liquide packaging example](/sources/images/carousel-two.png)

*3D model of a 10ml bottle of e-liquid inside the packagings carousel*

![Carousel demonstration](/sources/images/carousel-demo.gif)

*A short demo of the carousel (moving from one item to the other and manipuling it)* 
<br/>


[^1]: in France, short internships aren't necessarily paid