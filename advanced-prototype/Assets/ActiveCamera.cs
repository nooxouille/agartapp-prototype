﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class ActiveCamera : MonoBehaviour
{
    [SerializeField] GameObject zoomScriptContainer;
    [SerializeField] GameObject modelsCamera;

    private void Update()
    {
        ZoomControls();
    }

    public void ZoomControls()
    {
        if (modelsCamera.activeInHierarchy == false)
        {
            zoomScriptContainer.GetComponent<LeanCameraZoomSmooth>().enabled = false;
        }
        else if (modelsCamera.activeInHierarchy)
        {
            zoomScriptContainer.GetComponent<LeanCameraZoomSmooth>().enabled = true;
        }

    }
}
