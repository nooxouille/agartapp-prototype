﻿using System.Collections.Generic;
using UnityEngine;


/// <summary> Control the carousel movement depending on user input (buttons) </summary>
public class Carousel : MonoBehaviour
{
    [Header("List")]
    [Tooltip("Last item value must be the first 2 items's opposite")]
    [SerializeField] List<GameObject> models;

    [Header("Positions Targets")]
    [SerializeField] Vector3 posLeftTarget;
    [SerializeField] Vector3 posCenterTarget;
    [SerializeField] Vector3 posRightTarget;

    [Header("Buttons")]
    [Tooltip("Display buttons depending on models.Count")]
    [SerializeField] GameObject positiveButton;
    [SerializeField] GameObject negativeButton;
    // [SerializeField] GameObject switchButton;

    [Space]

    [Tooltip("Current index of the models List")]
    [SerializeField] int currentIndex = 0;
    [Tooltip("Flag for movement (-1/0/1)")]
    [SerializeField] int isMoving = 0;
    [Tooltip(" Move.Towards movement speed")]
    [SerializeField] float speed;
    [Tooltip(" Touch inputs")]
    [SerializeField] GameObject touchInputs;

    [Header("3D Camera")]
    [SerializeField] Camera modelsCamera;
    [SerializeField] float defaultFov;


    /*********************************************/

    /// <summary> Check the flag so the move.towards from Movement() can happen all at once </summary>
    private void Update()
    {
        // Locking the y and z position of both left and right targets positions to 0
        if (posRightTarget.y != 0 || posLeftTarget.y != 0)
        {
            posRightTarget.y = 0;
            posLeftTarget.y = 0;
        }

        if (posRightTarget.z != 0 || posLeftTarget.z != 0)
        {
            posRightTarget.z = 0;
            posLeftTarget.z = 0;
        }

        FreezeTouchInputs();

        if (isMoving != 0)
        {
            Movement();

        }
    }

    /// <summary> Display the appropriate buttons and set the current index as half the size models </summary>
    private void Start()
    {
        //DisplayButtons();
        currentIndex = Mathf.FloorToInt(models.Count / 2);
    }

    /// <summary> Check for the flag value </summary>
    /// <param name="mvt">Flag for the movement</param>
    public void MoveRequest(int mvt)
    {
        isMoving = (isMoving == 0) ? mvt : isMoving;
    }

    /// <summary> Remove or add the saved item of models depending on the movement flag  </summary>
    /// <param name="mvt"> movement flag, either positive movement (1) or negative (-1) </param>
    public void SwapIndex(int mvt)
    {
        GameObject svgItem;

        switch (mvt)
        {
            // Negative movement ; save the current first item, remove it from the list and add it back at the idem
            case -1:
                svgItem = models[0];
                models.RemoveAt(0);
                models.Add(svgItem);
                break;
            // Positive movement ; save the current last item, remove it from the list and add it back at the begining
            case 1:
                svgItem = models[models.Count - 1];
                models.RemoveAt(models.Count - 1);
                models.Insert(0, svgItem);
                break;
            default:
                break;
        }

        /* Depending on mvt, first or last item of models is tranformed to the left or to the right of the screen  
         * If mvt == 1 then first item move, else last item (models.count - 1) move 
         * then *****************************************************
         * If mvt == -1, destination is posLeftTarget else, destination is posRightTarget 
         *******************************************************************************************************/
        models[(mvt == 1) ? 0 : models.Count - 1].transform.position = (mvt == -1) ? posLeftTarget : posRightTarget;
    }

    /// <summary> Movement direction dependanding on the value of the isMoving flag </summary>
    public void Movement()
    {
        float step = speed * Time.deltaTime;
        Vector3 targetOut = (isMoving == -1) ? posRightTarget : posLeftTarget;
        Vector3 currentIndexPosition = models[currentIndex].transform.position;
        Vector3 previousIndexPosition = models[currentIndex - isMoving].transform.position;

        float delta = Vector3.Distance(currentIndexPosition, targetOut);

        float deltaRightPosition = Vector3.Distance(posCenterTarget, posRightTarget);
        float deltaLeftPosition = Vector3.Distance(posCenterTarget, posLeftTarget);

        // Move the <List>models</list>[currentIndex] from it's position, to the right
        models[currentIndex].transform.position = Vector3.MoveTowards(currentIndexPosition, targetOut, step);
        // Move the <List>models</list>[previoustIndex] from it's position, to the right
        models[currentIndex - isMoving].transform.position = Vector3.MoveTowards(previousIndexPosition, posCenterTarget, step);
        transform.rotation = Quaternion.identity;


        // Checking target positions 
        if (deltaRightPosition != 3 || deltaLeftPosition != 3)
        {
            posRightTarget.x = 3;
            posLeftTarget.x = -3;
        }


        // Check the distance between object and target so it can stop moving
        if (delta <= 0.02f)
        {
            SwapIndex(isMoving);
            isMoving = 0;
        }
    }

    /// <summary> If item is moving, freeze user touch inputs ; un-freeze if not moving</summary>
    public void FreezeTouchInputs()
    {
        if (isMoving != 0)
        {
            touchInputs.SetActive(false);
        }
        else if (isMoving == 0)
        {
            touchInputs.SetActive(true);
        }
    }

    public void ResetFov()
    {

            modelsCamera.fieldOfView = defaultFov;
            Debug.Log("Zoom reset");

        
    }

    /// <summary> Display button(s) depending on models.Count </summary>
    /*
    public void DisplayButtons()
    {
        if (models.Count <= 1)
        {
            positiveButton.SetActive(false);
            negativeButton.SetActive(false);
            switchButton.SetActive(false);
        }
        else if (models.Count == 2)
        {
            positiveButton.SetActive(false);
            negativeButton.SetActive(false);
            switchButton.SetActive(true);
        }
        else
        {
            switchButton.SetActive(false);
        }

    }
    */
}