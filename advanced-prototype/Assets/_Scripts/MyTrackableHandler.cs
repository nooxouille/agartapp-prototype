﻿using UnityEngine;
using Doozy.Engine;
using Vuforia;


// Require the gameObject to have a TrackableBehaviour component
// [RequireComponent(typeof(TrackableBehaviour))]

public class MyTrackableHandler : MonoBehaviour, ITrackableEventHandler
{
    TrackableBehaviour my_TrackableBehaviour;
    TrackableBehaviour.Status my_PreviousStatus;
    TrackableBehaviour.Status my_NewStatus;

    void Start()
    {
        my_TrackableBehaviour = GetComponent<TrackableBehaviour>();
        my_TrackableBehaviour.RegisterTrackableEventHandler(this);
    }

    /// <summary>
    /// Depending on the target, send a specific string used as a trigger for a view
    /// </summary>
    void ITrackableEventHandler.OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        my_PreviousStatus = previousStatus;
        my_NewStatus = newStatus;

        if (newStatus == TrackableBehaviour.Status.DETECTED ||newStatus == TrackableBehaviour.Status.TRACKED ||newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log("Trackable " + my_TrackableBehaviour.TrackableName + " found");
            switch (my_TrackableBehaviour.TrackableName)
            {
                case "packaging":
                    Debug.Log("Packaging ok");
                    GameEventMessage.SendEvent("packaging");
                    break;

                case "branding":
                    Debug.Log("Branding");
                    GameEventMessage.SendEvent("branding");
                    break;

                case "print":
                    Debug.Log("Print ok");
                    GameEventMessage.SendEvent("print");
                    break;

                case "edition":
                    Debug.Log("Edition");
                    GameEventMessage.SendEvent("edition");
                    break;

                case "web":
                    Debug.Log("Web");
                    GameEventMessage.SendEvent("web");
                    break;
            }
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED && newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            Debug.Log("Trackable " + my_TrackableBehaviour.TrackableName + " lost"); 
        }
    }
}