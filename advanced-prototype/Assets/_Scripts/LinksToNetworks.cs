﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinksToNetworks : MonoBehaviour
{
   public void GoTo(string url)
    {
        Application.OpenURL(url);
    }
}
