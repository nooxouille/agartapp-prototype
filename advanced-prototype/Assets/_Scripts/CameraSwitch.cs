﻿using TranslucentUI;
using UnityEngine;


public class CameraSwitch : MonoBehaviour
{
    [Header("Camera")]
    [SerializeField] GameObject vuforiaCamera;

    [Header("Views")]
    [SerializeField] GameObject scannerView;
    [SerializeField] GameObject subMenuView;

    void Update()
    {
        BlurryEffectControl();
    }

    public void BlurryEffectControl()
    {
        if (scannerView.activeInHierarchy)
        {
            vuforiaCamera.GetComponent<TranslucentUICamera>().enabled = false;
        }
        else if (scannerView.activeInHierarchy == false)
        {
            vuforiaCamera.GetComponent<TranslucentUICamera>().enabled = true;
        }
    }
}

// GameObject.Find("AR Camera").GetComponent<TranslucentUICamera>().enabled = true;
